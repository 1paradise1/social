import React from 'react'
import { useState } from 'react'
import axios from 'axios'

import './register.scss'
import { useDispatch, useSelector } from 'react-redux'
import { loginUser, registerUser } from '../../redux/actions/userActions'
import { Redirect } from 'react-router-dom'
const SignUp = () => {
    const dispatch = useDispatch()

    const isRegistred = useSelector(state => state.userReducer.isRegistred)

    const [userinfo, setInfo] = useState(
        {
            username: '',
            password: ''
        }
    )

    const hadlechange = e => {
        setInfo({
            ...userinfo,
            [e.target.name]: e.target.value
        })
    }

    const onusersubmit = e => {
        e.preventDefault()
        dispatch(registerUser(userinfo))
    }
    console.log(isRegistred)

    if (isRegistred) {
        dispatch(loginUser(userinfo))
        return <Redirect to='/home' />
    }
    if (!isRegistred) {
        return (
            <div className="signup">
                <div className="bg-container">
                    <div className="bg-center">
                        <h1 className="bg-title">S<span className="bg-key">o</span>cial</h1>
                        <p className="bg-body">Lorem ipsum dolor sit, amet consectetur adipisicing elit. At, doloremque.</p>
                        <p className="bg-body">Lorem ipsum dolor, sit amet consectetur adipisicing.</p>
                    </div>
                </div>
                <div className="signup-form">
                    <div className="signup-container">
                        <h1 className="signup-title">Registration</h1>
                        <div className="signup-form-container">
                            <form onSubmit={onusersubmit}>
                                <div className="form-item">
                                    <input type="text" className="signup-login" name="username" id="username" value={userinfo.username} onChange={hadlechange} />
                                    <label for="username" className="login-label">Login</label>
                                </div>
                                <div className="form-item">
                                    <input type="password" name="password" className="signup-password" value={userinfo.password} onChange={hadlechange} />
                                    <label for="password" className="login-label">Password</label>
                                </div>
                                <button type="submit">Sign Up</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SignUp