import React from 'react'
import { useSelector } from 'react-redux'

import './liked.scss'

const LikedPosts = () => {
    const isLiked = useSelector(state => state.postReducer.likedposts)

    return (
        <div className="liked-container">

            <div className="liked-list">
                <h4 className="section-title">
                    Liked Posts
                </h4>
                <div className="liked-posts">
                    {isLiked.posts && isLiked.posts.length > 0
                        ? isLiked.posts.map((post, i) => {
                            return <div key={i} className="liked-post">
                                <p className="liked-body">{post.body.substring(0, 50)}</p>
                                <h6 className="post-by">by {post.username}</h6>
                            </div>
                        })
                        : <div className="liked-post">
                            <p className="liked-body">No liked posts yet ...</p>
                        </div>
                    }
                </div>
            </div>
        </div>
    )
}

export default LikedPosts