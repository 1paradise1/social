import React from 'react'
import { useState } from 'react'

import './post.scss'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-regular-svg-icons'

import { useDispatch } from 'react-redux';
import { addcomment, deletepost } from '../../../redux/actions/postActions'
import { Comment } from '../comment/comment'

const Post = ({ post, liked, setlike }) => {

    const dispatch = useDispatch()

    const username = localStorage.getItem('user')

    const [comment, setComment] = useState('')

    const deletePost = postId => {
        dispatch(deletepost(postId))
    }

    const handleKeyPress = (e, postId) => {
        if (e.key === 'Enter') {
            e.preventDefault()
            dispatch(addcomment(postId, comment, username))
            setComment('')
        }
    }

    const handleChangeComment = e => {
        setComment(e.target.value)
    }

    return (

        <div className="post">
            <div className="post-author">
                <figure>
                    <img src={require('../../../images/1.png')} alt="" />
                </figure>
                <div className="author-info">
                    <div className="author-name">
                        <a href="#">{post.username ? post.username : 'Stas Diakun'}</a>

                    </div>
                    <span className="post-date">Published: {new Date(post.createdAt).toUTCString()}</span>
                </div>
            </div>
            <div className="post-body">
                <div className="post-image">
                    <img src={require('../../../images/user-post.jpg')} alt="" />
                </div>
                <div className="post-addlike">
                    <FontAwesomeIcon onClick={() => setlike(post)} className={liked ? 'like-icon' : ''} icon={faHeart} />
                    {post.username === username &&
                        <div className="delete-post">
                            <button onClick={() => deletePost(post._id)}>Delete</button>
                        </div>
                    }
                </div>
                <div className="post-description">
                    <p className="post-text">
                        {post.body}
                    </p>
                </div>
                <div className="comments">
                    {post.comments && post.comments.map((comment, i) => {
                        return <Comment key={i} comment={comment} id={post._id} />
                    })}
                    <div className="add-comment">
                        <div className="comment-author">
                            <figure>
                                <img src={require('../../../images/1.png')} alt="" />
                            </figure>
                        </div>
                        <form onKeyPress={(e) => handleKeyPress(e, post._id)} className="comment-form">
                            <textarea value={comment} onChange={handleChangeComment} placeholder="write a comment..." name="" id=""></textarea>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Post