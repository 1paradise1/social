import React from 'react'

import './main.scss'

import { useState } from 'react'
import { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { addpost, likepost, unlikepost, get_posts, } from '../../../redux/actions/postActions'
import Post from '../post/post'


const Main = () => {
    const dispatch = useDispatch()
    const username = localStorage.getItem('user')

    const [addedpost, setPost] = useState('')

    const posts = useSelector(state => state.postReducer.posts)
    const likedposts = useSelector(state => state.postReducer.likedposts)

    useEffect(() => {
        dispatch(get_posts(localStorage.getItem('id')))
    }, [dispatch])

    const handleChangePost = e => {
        setPost(e.target.value)
    }
    const submitPost = e => {
        e.preventDefault()
        dispatch(addpost(addedpost, username))
        setPost('')
    }

    const setlike = sendedpost => {
        let result = null
        if (likedposts && likedposts.hasOwnProperty('posts')) {
            result = likedposts.posts.filter(post => post.postId === sendedpost._id)
        }
        if (result != null && result.length > 0) {
            dispatch(unlikepost(sendedpost._id, localStorage.getItem('id')))
        } else {
            dispatch(likepost(sendedpost, localStorage.getItem('id')))
        }
    }

    const checkLike = (postId) => {
        let result = null
        if (likedposts && likedposts.hasOwnProperty('posts')) {
            result = likedposts.posts.filter(post => post.postId === postId)
        }
        if (result != null && result.length > 0) {
            return true
        } else {
            return false
        }
    }

    return (
        <div className="main-container">
            <div className="new-post">
                <div className="post-container">
                    <form onSubmit={submitPost}>
                        <div className="text-area">
                            <div className="user-info">
                                <figure>
                                    <img src={require('../../../images/1.png')} alt="" />
                                </figure>
                                <p className="user-title"><a href="">{username}</a></p>
                            </div>
                            <textarea placeholder="write something..." value={addedpost} onChange={handleChangePost}></textarea>
                            <div className="form-submit">
                                <button type="submit">Post</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
            <div className="posts-list">
                {posts.map((post, i) => {
                    let liked = checkLike(post._id)
                    return <Post key={i} post={post} liked={liked} setlike={setlike} />
                })}
            </div>
        </div>
    )
}

export default Main