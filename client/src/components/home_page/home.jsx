import React from 'react'
import LikedPosts from './liked_posts/liked'
import Main from './main/main'

import './home.scss'

const Home = () => {

    return (
        <div className="home-container">
            <Main />
            <LikedPosts />
        </div>
    )
}

export default Home