import React, { useEffect } from 'react'

import './comment.scss'

import { useDispatch, useSelector } from 'react-redux';
import { deletecomment, } from '../../../redux/actions/postActions'

export const Comment = ({ comment, id }) => {
    const dispatch = useDispatch()

    const username = localStorage.getItem('user')

    const deleteComment = (commentId, postId) => {
        dispatch(deletecomment(postId, commentId))
    }

    return (
        <div className="comment">
            <div className="comment-author">
                <figure>
                    <img src={require('../../../images/1.png')} alt="" />
                </figure>
            </div>
            <div className="comment-body">
                <div className="comment-head">
                    {comment.username ? comment.username : 'Jason Borne'}
                    {comment.username === username &&
                        <div className="delete-post">
                            <button onClick={() => deleteComment(comment._id, id)}>Delete</button>
                        </div>}
                </div>
                <p className="comment-text">{comment.body}</p>
            </div>
        </div>
    )
}