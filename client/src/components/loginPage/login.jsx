import React from 'react'
import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';

import '../registerPage/register.scss'
import { loginUser } from '../../redux/actions/userActions';
import { Redirect } from 'react-router-dom';
const SignIn = () => {
    const dispatch = useDispatch()
    const [userinfo, setInfo] = useState(
        {
            username: '',
            password: ''
        }
    )
    const currentUser = useSelector(state => state.userReducer.currentUser)

    const hadlechange = e => {
        setInfo({
            ...userinfo,
            [e.target.name]: e.target.value
        })
    }

    const onusersubmit = e => {
        e.preventDefault()
        dispatch(loginUser(userinfo))
    }

    if (Object.keys(currentUser).length > 0) {
        return <Redirect to="/home" />
    }

    if (Object.keys(currentUser).length === 0) {
        return (
            <div className="signup">
                <div className="signup-form">
                    <div className="signup-container">

                        <h1 className="signup-title">Login</h1>
                        <div className="signup-form-container">
                            <form onSubmit={onusersubmit}>
                                <div className="form-item">
                                    <input type="text" className="signup-login" name="username" id="username" value={userinfo.username} onChange={hadlechange} />
                                    <label for="username" className="login-label">Login</label>
                                </div>
                                <div className="form-item">
                                    <input type="password" name="password" id="password" className="signup-password" value={userinfo.password} onChange={hadlechange} />
                                    <label for="password" className="login-label">Password</label>
                                </div>
                                <button type="submit">Sign In</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="bg-container">
                    <div className="bg-center">
                        <h1 className="bg-title">S<span className="bg-key">o</span>cial</h1>
                        <p className="bg-body">Lorem ipsum dolor sit, amet consectetur adipisicing elit. At, doloremque.</p>
                        <p className="bg-body">Lorem ipsum dolor, sit amet consectetur adipisicing.</p>
                        <p className="bg-body">Lorem ipsum dolor, sit.</p>
                    </div>

                </div>
            </div>
        )
    }
}

export default SignIn