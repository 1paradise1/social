import React from 'react'
import './header.scss'
import {
    Link
} from "react-router-dom";
import { useDispatch } from 'react-redux';
import { logoutUser } from '../../redux/actions/userActions';
const Header = () => {
    const dispatch = useDispatch()

    const user = localStorage.getItem('user')

    return (
        <header className="header">
            <div className="header-container">
                <div className="header-title">
                    <h3 className="title">Social</h3>
                </div>
                {user
                    ? <div className="header-buttons">
                        <button className="header-buttons-item"><Link className="button-link" onClick={() => dispatch(logoutUser())} >Logout</Link></button>
                    </div>
                    : <div className="header-buttons">
                        <button className="header-buttons-item"><Link className="button-link" to="/signin" >Sign In</Link></button>
                        <button className="header-buttons-item"><Link className="button-link" to="/signup" >Sign Up</Link></button>
                    </div>
                }
            </div>
        </header>
    )
}

export default Header