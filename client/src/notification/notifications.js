import { NotificationManager } from 'react-notifications';

export const createNotification = (message) => {
    switch (message.type) {
        case 'success':
            NotificationManager.success('Success message', message.text)
            break
        case 'error':
            NotificationManager.error('Error message', message.text)
            break
    }
}