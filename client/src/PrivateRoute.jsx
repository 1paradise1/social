import React from 'react'
import { Redirect, Route } from 'react-router-dom'

const PrivateRoute = ({ component: Components, ...rest }) => {

    return (
        <Route
            {...rest}
            render={props =>
                localStorage.getItem("token")
                    ? (
                        Components.map((Component, i) => {
                            return <Component key={i} {...props} />
                        })
                    )
                    : (
                        <Redirect
                            to="/signin"
                        />
                    )
            }
        />
    )
}

export default PrivateRoute