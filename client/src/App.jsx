import React from 'react';
import './App.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import { NotificationContainer } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import Header from './components/header/header';
import SignUp from './components/registerPage/register';
import SignIn from './components/loginPage/login';
import Home from './components/home_page/home';
import PrivateRoute from './PrivateRoute';

function App() {
  return (
    <Router>
      <div className="container">
        <Switch>
          <Route path="/signin">
            <SignIn />
          </Route>
          <Route path="/signup">
            <SignUp />
          </Route>
          <PrivateRoute exact path="/home" component={[Header, Home]} />
        </Switch>
        <NotificationContainer />
      </div>
    </Router>
  );
}

export default App;
