import axios from "axios"
import { createNotification } from "../../notification/notifications"
import { LOGIN_USER, LOGOUT_USER, REGISTER_USER } from "../types"

export const registerUser = (userinfo) => async dispatch => {
    try{
        const response = await axios.post('/user/register', userinfo)
        const payload = response.data
        dispatch({
            type: REGISTER_USER,
            payload: payload.isRegister
        })
        createNotification(payload.message)
    }catch(err){
        dispatch({
            type: REGISTER_USER,
            payload: false
        })
        createNotification({ type: 'error', text: 'error, please try again' })
    }
}

export const loginUser = (userdata) => async dispatch => {
    const response = await axios.post('/user/login', userdata)
    const payload = await response.data

    localStorage.setItem('token', payload.token)
    localStorage.setItem('user', payload.user.username)
    localStorage.setItem('id', payload.user.id)

    dispatch({
        type: LOGIN_USER,
        payload
    })
}


export const logoutUser = () => async dispatch => {
    localStorage.clear()

    dispatch({
        type: LOGOUT_USER,
    })
}
