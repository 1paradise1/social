import axios from "axios"
import { createNotification } from "../../notification/notifications"
import { ADD_POST, GET_POSTS, LIKE_POST, UNLIKE_POST, ADD_COMMENT, DELETE_POST, DELETE_COMMENT, GET_ALLPOSTS } from "../types"


export const get_posts = (payload) => async dispatch => {
    try {
        const response = await axios.post('/post/getall', { userId: payload })
        const result = await response.data
        if(response.data.likedPosts != null){
            dispatch({
                type: GET_ALLPOSTS,
                payload: {
                    posts: result.posts.reverse(),
                    likedposts: result.likedPosts
                }
            })
        }else{
            dispatch({
                type: GET_POSTS,
                payload: {
                    posts: result.posts.reverse(),
                }
            })
        }
    } catch (err) {
        createNotification({ type: 'error', text: 'error, please refresh the page' })
    }
}

export const addpost = (post, username) => async dispatch => {
    try {
        const response = await axios.post('/post/add', { post, username })
        const result = await response.data
        dispatch({
            type: ADD_POST,
            payload: result.post
        })
        createNotification(result.message)
    } catch (err) {
        createNotification({ type: 'error', text: 'error, please try again' })
    }
}

export const deletepost = (postId) => async dispatch => {
    const userId = localStorage.getItem('id')
    try {
        const response = await axios.post(`/post/${postId}/delete`, { userId })
        const result = await response.data

        dispatch({
            type: DELETE_POST,
            payload: postId
        })
        createNotification(result.message)
    } catch (err) {
        console.log(err);
        createNotification({ type: 'error', text: 'error, please try again' })
    }

}


export const likepost = (post, userid) => async dispatch => {
    try {
        const response = await axios.post(`/post/${post._id}/like`, { userid, post })
        const result = await response.data

        dispatch({
            type: LIKE_POST,
            payload: result.liked
        })
        createNotification(result.message)
    } catch (err) {
        createNotification({ type: 'error', text: 'error, please try again' })
    }
}

export const unlikepost = (id, userid) => async dispatch => {
    try {
        const response = await axios.post(`/post/${id}/unlike`, { userid })
        const result = await response.data
        dispatch({
            type: UNLIKE_POST,
            payload: id
        })
        createNotification(result.message)
    } catch (err) {
        createNotification({ type: 'error', text: 'error, please try again' })
    }
}

export const addcomment = (id, commentbody, username) => async dispatch => {
    try {
        const response = await axios.post(`/post/${id}/addcomment`, { commentbody, username })
        const result = await response.data

        dispatch({
            type: ADD_COMMENT,
            payload: {
                comments: result.comments,
                id
            }
        })
        createNotification(result.message)
    } catch (err) {
        createNotification({ type: 'error', text: 'error, please try again' })
    }

}

export const deletecomment = (id, commentId) => async dispatch => {
    try {
        const response = await axios.post(`/post/${id}/deletecomment`, { commentId })
        const result = await response.data

        dispatch({
            type: DELETE_COMMENT,
            payload: {
                id,
                commentId
            }
        })
        createNotification(result.message)

    } catch (err) {
        createNotification({ type: 'error', text: 'error, please try again' })
    }

}
