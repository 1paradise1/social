import { ADD_COMMENT, ADD_POST, DELETE_COMMENT, DELETE_POST, GET_ALLPOSTS, GET_POSTS, LIKE_POST, UNLIKE_POST } from "../types"

const initialState = {
    posts: [],
    likedposts: {},
}

const postReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_POSTS:
            return { ...state, posts: action.payload.posts }
        case GET_ALLPOSTS:
            return { ...state, posts: action.payload.posts, likedposts: action.payload.likedposts }
        case ADD_POST:
            return { ...state, posts: [action.payload, ...state.posts] }
        case DELETE_POST: {
            const result = state.likedposts.posts.filter(post => post.postId !== action.payload)
            const newlikedposts = Object.assign({}, state.likedposts)
            newlikedposts.posts = result
            return { ...state, posts: state.posts.filter(post => post._id !== action.payload), likedposts: newlikedposts }
        }
        case LIKE_POST:
            return { ...state, likedposts: action.payload }
        case UNLIKE_POST: {
            const result = state.likedposts.posts.filter(post => post.postId !== action.payload)
            const newlikedposts = Object.assign({}, state.likedposts)
            newlikedposts.posts = result
            return { ...state, likedposts: newlikedposts }
        }
        case ADD_COMMENT: {
            const index = state.posts.findIndex(post => post._id === action.payload.id);
            const newPosts = [...state.posts]
            newPosts[index].comments = action.payload.comments
            return { ...state, posts: newPosts }
        }
        case DELETE_COMMENT: {
            const postindex = state.posts.findIndex(post => post._id === action.payload.id)
            const deletedPosts = [...state.posts]
            const result = deletedPosts[postindex].comments.filter(comment => comment._id !== action.payload.commentId)
            deletedPosts[postindex].comments = result
            return { ...state, posts: deletedPosts }
        }
        default: return state
    }
}

export default postReducer