import {LOGIN_USER, LOGOUT_USER, REGISTER_USER }  from "../types"

const initialState = {
    currentUser: {},
    isRegistred: false
}

const userReducer = (state=initialState, action) =>{
    switch(action.type){
        case REGISTER_USER:
            return {...state, isRegistred: action.payload}
        case LOGIN_USER:
            return {...state, currentUser: action.payload.user}
        case LOGOUT_USER:
            return{ ...state, currentUser: {}}
        default: return state
    }
}

export default userReducer