const router = require('express').Router();
const User = require('../models/user.model');

const path = require("path");
const multer = require("multer");

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now())
    }
});

const upload = multer({
    storage: storage,
})

router.route('/register').post((req, res) => {
    const { username, password } = req.body
    const user = {
        username: username,
        password: password,
        bio: '',
        image: '',
        posts: []
    }

    const newUser = new User(user)
    newUser.save((err, saved) => {
        if (err) {
            res.status(500).send('error, please try again')
        } else {
            res.json({
                message: {
                    type: 'success',
                    text: 'User registred'
                },
                isRegister: true
            })
        }
    })
});

router.post('/login', (req, res) => {
    const { username, password } = req.body
    User.findOne({ username }, (err, user) => {
        if (err || !user) {
            res.status(401)
                .json({
                    error: 'Incorrect email or password'
                });
        } else {
            bcrypt.compare(password, user.password, function (err, same) {
                if (err) {
                    res.status(500)
                        .json({
                            error: 'Internal error please try again'
                        });
                } else {
                    jwt.sign({ username }, 'secretkey', { expiresIn: '1h' }, (err, token) => {
                        res.json({
                            token,
                            user: {
                                id: user.id,
                                username: user.username,
                            }
                        });
                    });
                }
            });
        }
    })
});

router.get('/:id', (req, res) => {
    const id = req.params.id
    User.findById(id, (err, user) => {
        if (err || !user) {
            res.json({
                error: 'error please try again'
            });
        } else {
            res.json({
                user: {
                    username: user.username,
                    bio: user.bio,
                    posts: user.posts,
                }
            })
        }
    })
});


module.exports = router;