const router = require('express').Router();
const Post = require('../models/post.model');
const LikePost = require('../models/likedpost.model');

router.post('/add', (req, res) => {
    const post = {
        body: req.body.post,
        comments: [],
        image: '',
        likes: 0,
        username: req.body.username
    }

    const newPost = new Post(post);
    newPost.save()
        .then((post) => {
            res.json({
                message: {
                    type: 'success',
                    text: 'Post added'
                },
                post
            })
        }
        )
        .catch(res => res.status(500).send('error, please try again'));
})

router.post('/:id/delete', (req, res) => {
    Post.findByIdAndDelete(req.params.id, (err, post) => {
        if (err) {
            res.status(500).send('error, please try again')
        } else {
            LikePost.findOneAndUpdate({ userId: req.body.userId }, { $pull: { posts: { postId: req.params.id } } }, (err, lpost) => {
                res.json({
                    message: {
                        type: 'success',
                        text: 'Post deleted'
                    }
                })
            })
        }
    })
})

router.post('/getall', (req, res) => {
    Post.find({}, (err, posts) => {
        if (err) {
            res.status(500).send('error, please try again')
        } else {
            LikePost.find({ userId: req.body.userId }, (err, lposts) => {
                if (err || !lposts) {
                    res.json({
                        posts,
                        likedPosts: null
                    })
                } else {
                    res.json({
                        posts,
                        likedPosts: lposts[0]
                    })
                }
            })
        }
    })
})

router.post('/:id/like', (req, res) => {
    LikePost.findOneAndUpdate({ userId: req.body.userid }, { userId: req.body.userid, $push: { posts: { postId: req.params.id, body: req.body.post.body, username: req.body.post.username } } }, { upsert: true, new: true }, (err, liked) => {
        if (err) {
            res.status(500).send('error, please try again')
        } else {
            res.json({
                message: {
                    type: 'success',
                    text: 'Liked'
                },
                liked
            })
        }
    })


})

router.post('/:id/unlike', (req, res) => {
    LikePost.findOneAndUpdate({ userId: req.body.userid }, { $pull: { posts: { postId: req.params.id } } }, (err, liked) => {
        if (err || !liked) {
            res.status(500).send('error, please try again')
        } else {
            res.json({
                message: {
                    type: 'success',
                    text: 'Unliked'
                }
            })
        }
    })
})

router.post('/:id/addcomment', (req, res) => {
    const id = req.params.id
    Post.findByIdAndUpdate(id, { $push: { comments: { username: req.body.username, body: req.body.commentbody } } }, { new: true }, (err, post) => {
        if (err || !post) {
            res.status(500).send('error, please try again')
        } else {
            res.json({
                message: {
                    type: 'success',
                    text: 'Comment added'
                },
                comments: post.comments
            })
        }
    })
})

router.post('/:id/deletecomment', (req, res) => {
    Post.findByIdAndUpdate(req.params.id, { $pull: { comments: { _id: req.body.commentId } } }, (err, del) => {
        if (err || !del) {
            res.status(500).send('error, please try again')
        } else {
            res.json({
                message: {
                    type: 'success',
                    text: 'Comment deleted'
                }
            })
        }
    })
})


module.exports = router;