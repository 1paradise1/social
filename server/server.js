const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }
);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
})

const userSchema = require('./routes/user')
const postSchema = require('./routes/post')

app.use('/user', userSchema)
app.use('/post', postSchema)

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});