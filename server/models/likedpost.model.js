const mongoose = require("mongoose");


const likedpostSchema = new mongoose.Schema({
    userId: mongoose.Types.ObjectId,
    posts: [{
        postId: {
            type: mongoose.Types.ObjectId,
            unique: true
        },
        body: String,
        username: String
    }],
}, {
    timestamps: true,
});




module.exports = mongoose.model('LikedPost', likedpostSchema);


