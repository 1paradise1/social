const mongoose = require("mongoose");


const postSchema = new mongoose.Schema({
    body: String,
    comments: [{
        username: String,
        body: String
    }],
    image: {
        data: Buffer,
        contentType: String
    },
    username: String
}, {
    timestamps: true,
});




module.exports = mongoose.model('Post', postSchema);


